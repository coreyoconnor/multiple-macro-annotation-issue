package example

import example.macros._

@Foo
case class A(name: String)

@Bar
case class B(name: String)

@Foo
@Bar
case class C(name: String)

@Bar
@Foo
case class D(name: String)

@Foo
@Bar
case class E(name: String)
object E {}

@Bar
@Foo
case class F(name: String)
object F {}

object Hello extends App {
  println(A.doFoo)
  println(B.doBar)
  println(C.doBar)
}
