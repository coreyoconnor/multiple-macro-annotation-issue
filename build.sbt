import Dependencies._

ThisBuild / scalaVersion     := "2.13.11"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "com.example"
ThisBuild / organizationName := "example"

lazy val macros = (project in file("macros"))
  .settings(
    name := "macros",
    scalacOptions += "-Ymacro-annotations",
    libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value,
    libraryDependencies += munit % Test
  )

lazy val root = (project in file("."))
  .dependsOn(macros)
  .settings(
    name := "app",
    scalacOptions += "-Ymacro-annotations",
    libraryDependencies += munit % Test
  )
