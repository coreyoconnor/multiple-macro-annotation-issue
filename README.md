execute `run` in sbt. 

note the crash:

```
Caused by: java.lang.ClassNotFoundException: example.C$
at java.base/java.net.URLClassLoader.findClass(URLClassLoader.java:436)
at sbt.internal.ManagedClassLoader.findClass(ManagedClassLoader.java:103)
at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:588)
at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:521)
... 31 more
```

Note the `C$.class` is not created as expected.
