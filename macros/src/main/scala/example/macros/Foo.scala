package example.macros

import scala.annotation.{StaticAnnotation, compileTimeOnly}
import scala.language.experimental.macros
import scala.language.postfixOps
import scala.reflect.macros.whitebox

class Foo extends StaticAnnotation{
  def macroTransform(annottees: Any*): Any = macro Foo.impl
}

object Foo {
  def impl(c: whitebox.Context)(annottees: c.Expr[Any]*): c.Expr[Any] = {
    import c.universe._

    def addFooFunctionality(module: Tree): Tree = {
      val fooerType = weakTypeOf[Fooer].typeSymbol

      module match {
        case q"object $className { ..$body }" =>
          q"""
              object $className extends $fooerType {
                ..$body

                val doFoo: String = "Foo"
              }
           """
        case q"object $className extends ..$parents { ..$body }" =>
          q"""
             object $className extends ..$parents with $fooerType {
               ..$body

               val doFoo: String = "Foo"
             }
           """
        case _ => c.abort(c.enclosingPosition, "Module does not match")
      }
    }

    val result = annottees map (_.tree) toList match {
      case (classDef @ ClassDef(cMods, cName, _, _)) :: tail =>
        val moduleDef = tail match {
          case (md @ ModuleDef(_, mName, _)) :: Nil if cName.decodedName.toString == mName.decodedName.toString => md
          case Nil =>
            q"""
             object ${cName.toTermName} {

             }
             """
          case e => c.abort(c.enclosingPosition, s"Foo annotation only works on classes. Not $e.")
        }
        val modifiedModuleDef = addFooFunctionality(moduleDef)

        c.Expr[Fooer](q"""
            $classDef
            $modifiedModuleDef
          """)

      case e => c.abort(c.enclosingPosition, s"Foo annotation only works on classes. Not $e.")
    }

    result
  }
}


trait Fooer {
  val doFoo : String
}
