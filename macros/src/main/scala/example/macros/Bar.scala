package example.macros

import scala.annotation.{StaticAnnotation, compileTimeOnly}
import scala.language.experimental.macros
import scala.language.postfixOps
import scala.reflect.macros.whitebox

class Bar extends StaticAnnotation{
  def macroTransform(annottees: Any*): Any = macro Bar.impl
}

object Bar {
  def impl(c: scala.reflect.macros.whitebox.Context)(annottees: c.Expr[Any]*): c.Expr[Any] = {
    import c.universe._

    def addBarFunctionality(module: Tree): Tree = {
      val barerType = weakTypeOf[Barer].typeSymbol

      module match {
        case q"object $className { ..$body }" =>
          q"""
              object $className extends $barerType {
                ..$body

                val doBar: String = "Bar"
              }
           """

        case q"object $className extends ..$parents { ..$body }" =>
          q"""
             object $className extends ..$parents with $barerType {
               ..$body

               val doBar: String = "Bar"
             }
           """
      }
    }

    annottees map (_.tree) toList match {
      case (classDef @ ClassDef(cMods, cName, _, _)) :: tail =>
        val moduleDef = tail match {
          case (md @ ModuleDef(_, mName, _)) :: Nil if cName.decodedName.toString == mName.decodedName.toString => md
          case Nil =>
            q"""
             object ${cName.toTermName} {

             }
             """
          case e => c.abort(c.enclosingPosition, s"Bar annotation only works on classes. Not $e.")
        }
        val modifiedModuleDef = addBarFunctionality(moduleDef)

        c.Expr(q"""
            $classDef
            $modifiedModuleDef
          """)

      case e => c.abort(c.enclosingPosition, s"Bar annotation only works on classes. Not $e.")
    }
  }
}

trait Barer {
  val doBar : String
}
